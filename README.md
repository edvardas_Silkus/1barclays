This is Codility task for Barclays 1 task (3h 30min is given to complete tasks 1 and 2).

Task is simple there is a plane it can have a set number of rows.
In each row there are 10 seats, there is a gap between seat 3 and 4 as well a gap between 7 and 8.
So it looks something like this.

XXX XXXX XXX
XXX XXXX XXX
XXX XXXX XXX
XXX XXXX XXX
XXX XXXX XXX

Some places are reserved. Your task is to count how many families of 3 can be seated in this plane.
Knowing that all family has to be together. 
It means that family cannot be separated by a gap or a reserved spot.
