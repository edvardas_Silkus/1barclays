/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Barclays_1task;

/**
 *
 * @author Edvar
 */
public class Barclays_1task {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Starting data
        String S = "1A 3C 2B 40G 40E 5A";
        int N = 40;
        //S lenght is M

        //Plane seat data creation
        boolean[][] plane = new boolean[N][10];

//        for testing
//        for(int i = 0; i< N; i++){
//            for(int j = 0; j< 10; j++){
//            System.out.print(" " + plane[i][j]);
//            }
//            System.out.println(" ");
//        }
        if(S.isEmpty()){
            S = " ";
        }
        String[] parts = S.split(" ");

        for (int i = 0; i < parts.length; i++) {
            String[] seperatedParts = parts[i].split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");

//            For testing
//            System.out.println(seperatedParts[0]);
//            System.out.println(seperatedParts[1]);
            int part1 = Integer.parseInt(seperatedParts[0]);
            int part2 = 10;
            switch (seperatedParts[1].toLowerCase()) {
                case "a":
                    part2 = 0;
                    break;
                case "b":
                    part2 = 1;
                    break;
                case "c":
                    part2 = 2;
                    break;
                case "d":
                    part2 = 3;
                    break;
                case "e":
                    part2 = 4;
                    break;
                case "f":
                    part2 = 5;
                    break;
                case "g":
                    part2 = 6;
                    break;
                case "h":
                    part2 = 7;
                    break;
                case "j":
                    part2 = 8;
                    break;
                case "k":
                    part2 = 9;
                    break;
                default:
                    System.out.println("Error - String reading");
                    break;
            }
//            For testing
//            System.out.print("part1 = " + part1);
//            System.out.println(" part2 = " + part2);
            if(part1 <= N){
                plane[part1-1][part2] = true;
            }
            
        }

        //Counting seats
        int availableFamilySeats = 0;

        for (int i = 0; i < N; i++) {
            //Left row
            if (!plane[i][0] && !plane[i][1] && !plane[i][2]) {
                availableFamilySeats++;
            }
            //Right row
            if (!plane[i][9] && !plane[i][8] && !plane[i][7]) {
                availableFamilySeats++;
            }
            //Mid row
            if (!plane[i][3]) {//if first seat of the row is free
                if (!plane[i][3] && !plane[i][4] && !plane[i][5]) {
                    availableFamilySeats++;
                }
            } else {//if first seat of the row is taken
                if (!plane[i][4] && !plane[i][5] && !plane[i][6]) {
                    availableFamilySeats++;
                }
            }

        }

        System.out.println(availableFamilySeats);//need to erase
    }
}
